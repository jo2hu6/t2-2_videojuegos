using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MegamanController : MonoBehaviour
{

    public GameObject BulletSmallRight;
    public GameObject BulletMediumRight;
    public GameObject BulletBigRight;

    private float switchColorDelay = 0.1f;
    private float switchColorTime = 0f;
    private Color originalColor;

    private int contador = 0;
    private bool puedeSaltar = false;
    private bool isReload = false;

     private SpriteRenderer sr;
    private Animator _animator;
    private Rigidbody2D rb2d;

    void Start()
    {
        sr = GetComponent<SpriteRenderer>();
        _animator = GetComponent<Animator>();
        rb2d = GetComponent<Rigidbody2D>();
        originalColor = sr.color;
    }

    // Update is called once per frame
    void Update()
    {
        
        //BALAS
        if(Input.GetKeyDown(KeyCode.C))
        {
            var position = new Vector2(transform.position.x + 1, transform.position.y);
            Instantiate(BulletSmallRight, position, BulletSmallRight.transform.rotation);
        }

        if (Input.GetKeyDown(KeyCode.X) && isReload == false)
		{
			SwitchColor();
			isReload = true;

			if (isReload == true)
			{
				Invoke("EndReloadMediumBullet", 3);
			}

		}

        if (Input.GetKeyDown(KeyCode.Z) && isReload == false)
		{
			SwitchColor();
			isReload = true;

			if (isReload == true)
			{
				Invoke("EndReloadBigBullet", 5);
			}

		}

        //DESPLAZARSE
        if (Input.GetKey(KeyCode.RightArrow))
        {
            sr.flipX= false;
            setRunAnimation();
            rb2d.velocity = new Vector2(12,rb2d.velocity.y);
        }
        else
        {
            setIdleAnimation();
            rb2d.velocity = new Vector2(0,rb2d.velocity.y);
        }

        if (Input.GetKey(KeyCode.LeftArrow))
        {
            sr.flipX = true;
            setRunAnimation();
            rb2d.velocity = new Vector2(-12,rb2d.velocity.y);
        }


        //SALTAR
        if (Input.GetKey(KeyCode.Space))
        {
            setJumpAnimation();
            if(puedeSaltar == true){
                float upSpeed = 40;
                rb2d.velocity = Vector2.up * upSpeed;
            }
            puedeSaltar = false;
        }

        

    }


    private void SwitchColor()
    {
        if(sr.color == originalColor)
            sr.color = Color.yellow;
        else
            sr.color = originalColor;
        switchColorTime = 0;
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        if(other.gameObject.tag == "Floor")
        {
            puedeSaltar = true;
        }
    }

    private void EndReloadMediumBullet()
    {
        sr.color = originalColor;
        var position = new Vector2(transform.position.x + 1, transform.position.y);
        Instantiate(BulletMediumRight, position, BulletMediumRight.transform.rotation);
        isReload=false;
    }

    private void EndReloadBigBullet()
    {
        sr.color = originalColor;
        var position = new Vector2(transform.position.x + 1, transform.position.y);
        Instantiate(BulletBigRight, position, BulletBigRight.transform.rotation);
        isReload = false;
    }

    private void setAppearAnimation(){
        _animator.SetInteger("State",0);
    }

    private void setIdleAnimation(){
        _animator.SetInteger("State",1);
    }

     private void setRunAnimation(){
       _animator.SetInteger("State",2);
    }

    private void setRunShootAnimation(){
        _animator.SetInteger("State",3);
    }

    private void setJumpAnimation(){
        _animator.SetInteger("State",4);
    }

    private void setJumpShootAnimation(){
        _animator.SetInteger("State",5);
    }


}
